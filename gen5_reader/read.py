

import pandas

def read(filename):
    return pandas.read_csv(filename, delimiter="\t", decimal=",")
