#!/usr/bin/env python
# coding: utf-8

import argparse
import logging
import sys
import re
import os.path

## set up basic logging
logging.basicConfig(level=logging.INFO)

def extract_frames(filename):
    logger = logging.getLogger(__name__)

    logger.info("reading {}".format(filename))
    with open(filename) as f:
        lines = f.readlines()
    logger.info("read {} lines".format(len(lines)))

    stack = []
    frame_begins = False
    for line in lines:
        logger.debug(line)

        stack.append(line)


        if frame_begins == False and line.strip(" \n\r\f\v") != "":
            frame_begins = True
            stack.append(stack.pop().strip())

            logger.debug("frame_begins = True")
            continue

        if frame_begins == True and len(stack) == 2 and line.strip(" \n\r\f\v") == "":
            # empty line between header and frame
            # remove
            stack.pop()

            logger.debug("stack.pop()")
            continue

        if frame_begins == True and len(stack) > 2 and line.strip(" \n\r\f\v") == "":
            # done
            frame_name = stack[0]
            frame_content = stack[1:]

            logger.debug("yield frame_name, frame_content")
            logger.debug(frame_name)
            logger.debug(frame_content)

            stack = []
            logger.debug("stack = []")

            frame_begins = False
            logger.debug("frame_begins = False")

            yield frame_name, frame_content

def split(filename, separator_change=False, not_in_place=False):
    logger = logging.getLogger(__name__)

    all_frames = list(extract_frames(filename))
    logger.info("extracted {} frames".format(len(all_frames)))

    if not_in_place:
        filename = os.path.basename(filename)

    for name, content in all_frames:

        old_name = name

        name = re.sub('[^0-9a-zA-Z]+', '_', name)
        if old_name != name:
            logger.debug("Needed to regex a file character replacement: ")
            logger.debug(str(old_name) + " became " +  str(name))


        with open("{}_{}.tsv".format(filename[:-4],name), "w") as g:
            logger.info("Writing {}_{}.tsv".format(filename[:-4],name))
            if separator_change == True:
                ## replace all comma with decimal points
                content = [re.sub(r'([0-9]),([0-9])', r'\1.\2', entry) for entry in content]
            g.writelines(content)

    return

def main(argv=None):

    logger = logging.getLogger(__name__)

    if argv is None:
        argv = sys.argv[1:]


    ## prepare ArgumentParser
    parser = argparse.ArgumentParser(description="", epilog="", add_help=True)

    parser.add_argument(dest="filenames", metavar="<file_to_split>", nargs="+", help="")
    parser.add_argument("-s", "--separator", action="store_true", help="changes the decimal separator from comma (1,29) to point (1.29)")
    parser.add_argument('-v', '--verbose', dest='debug', action='store_true', help='Show verbose information.')
    parser.add_argument('-n', '--not_in_place',action='store_true', help='Saves output files into current directory instead of the original path.')

    ## processes args from ArgumentParser
    args = parser.parse_args(argv)

    if args.debug:
        logger.setLevel(logging.DEBUG)

    filenames = args.filenames
    separator_change = args.separator
    not_in_place = args.not_in_place


    logger.info("Processing files...")

    for filename in filenames:
        logger.info(filename)
        split(filename, separator_change, not_in_place)

if __name__ == "__main__":
    main()
