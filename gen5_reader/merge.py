
import pandas
import gen5_reader
import argparse
import logging
import sys
import os
import fnmatch
if sys.version_info[0] < 3:
    from StringIO import StringIO
else:
    from io import StringIO

def merge(filename):
    all_frames = list(gen5_reader.split.extract_frames(filename))
    df_dict = {}
    for frame in all_frames:
        print(frame)
        stringio = StringIO("".join(frame[1]))
        df_dict.update( { frame[0] : gen5_reader.read.read(stringio) } )
    return pandas.Panel(df_dict)

def main(argv=None):
    if argv is None:
        argv = sys.argv

    ## set up basic logging
    logging.basicConfig(level=logging.INFO)

    ## prepare ArgumentParser
    parser = argparse.ArgumentParser(description="", epilog="", add_help=True)

    parser.add_argument(dest="filenames", metavar="<filename_to_convert>", nargs="+", help="")

    ## processes args from ArgumentParser
    args = parser.parse_args(argv[1:])

    filenames = args.filenames

    for filename in filenames:
        df = merge(filename)
        df.to_frame().to_csv(filename[:-4]+".csv")

if __name__ == "__main__":
    main()
